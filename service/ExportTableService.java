package com.hjb.manager.common.export.service;

import java.util.List;

import com.hjb.manager.common.export.model.ExportTable;



/**
 * 
 * 项目名称：manager    
 * 类名称：ExportService    
 * 类描述：    
 * 创建人：田浩宇    
 * 创建时间：2017年8月4日 上午11:01:15    
 * 修改人：田浩宇 17743566685@sina.cn     
 * 修改时间：2017年8月4日 上午11:01:15    
 * 修改备注：       
 * @version
 */
public interface ExportTableService {
	
	public ExportTable queryExportTable(String tableName)throws Exception;
		
}
