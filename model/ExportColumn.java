package com.hjb.manager.common.export.model;


/**
 * 
 * 项目名称：manager    
 * 类名称：ExportColumn    
 * 类描述：    
 * 创建人：田浩宇    
 * 创建时间：2017年8月4日 上午11:01:01    
 * 修改人：田浩宇 17743566685@sina.cn     
 * 修改时间：2017年8月4日 上午11:01:01    
 * 修改备注：       
 * @version
 */
public class ExportColumn {
	
	private String tableName;
	private String columnName;
	private String columnComment = "" ;
	private String nallable = ""; 
	private String columnType = ""; //VARCHAR2 .....
	private String columnLength = "";
	private String defaultValue = "";
	
	
	
	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getColumnComment() {
		return columnComment;
	}
	public void setColumnComment(String columnComment) {
		this.columnComment = columnComment;
	}
	
	
	public String getNallable() {
		return nallable;
	}
	public void setNallable(String nallable) {
		this.nallable = nallable;
	}
	public String getColumnType() {
		return columnType;
	}
	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}
	public String getColumnLength() {
		return columnLength;
	}
	public void setColumnLength(String columnLength) {
		this.columnLength = columnLength;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	@Override
	public String toString() {
		return "ExportColumn [tableName=" + tableName + ", columnName="
				+ columnName + ", columnComment=" + columnComment
				+ ", nallable=" + nallable + ", columnType=" + columnType
				+ ", columnLength=" + columnLength + ", defaultValue="
				+ defaultValue + "]";
	}
	
	
	
}
