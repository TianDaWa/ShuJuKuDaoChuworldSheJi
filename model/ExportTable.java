package com.hjb.manager.common.export.model;

import java.util.List;

import com.hjb.common.base.model.BaseModel;




/**
 * 
 * 项目名称：manager    
 * 类名称：ExportTable    
 * 类描述：    
 * 创建人：田浩宇    
 * 创建时间：2017年8月4日 上午11:01:06    
 * 修改人：田浩宇 17743566685@sina.cn     
 * 修改时间：2017年8月4日 上午11:01:06    
 * 修改备注：       
 * @version
 */
public class ExportTable {
	
	private String tableComment;
	private String tableName;
	private List<ExportColumn> columns;
	
	
	
	public List<ExportColumn> getColumns() {
		return columns;
	}
	public void setColumns(List<ExportColumn> columns) {
		this.columns = columns;
	}
	public String getTableComment() {
		return tableComment;
	}
	public void setTableComment(String tableComment) {
		this.tableComment = tableComment;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	@Override
	public String toString() {
		return "ExportTable [tableComment=" + tableComment + ", tableName="
				+ tableName + ", columns=" + columns + "]";
	}
	
}
