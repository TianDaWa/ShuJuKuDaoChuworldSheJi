package com.hjb.manager.common;

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.hjb.manager.common.export.model.ExportColumn;
import com.hjb.manager.common.export.model.ExportTable;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.rtf.RtfWriter2;

public class TestExport {

	public static void main(String[] args) throws Exception {
		TestExport word = new TestExport();  
        String file = "/home/bigtree/workspace/test.doc";  
        //设置纸张大小  
        Document document = new Document(PageSize.A4);  
        //建立一个书写器，与document对象关联  
        RtfWriter2.getInstance(document, new FileOutputStream(file));  
        document.open();  
        //设置中文字体  
        BaseFont bfChinese = BaseFont.createFont("/home/bigtree/workspace/simsun.ttc,0", BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
        //标题字体风格  
        Font titleFont = new Font(bfChinese,12,Font.BOLD);  
        //正文字体风格  
        Font contextFont = new Font(bfChinese,10,Font.NORMAL);  
        Paragraph title = new Paragraph("数据库表结构设计说明书");  
        //设置标题格式对齐方式  
        title.setAlignment(Element.ALIGN_CENTER);  
        title.setFont(titleFont);  
        document.add(title);
        try {  
        	List<ExportTable> datas = makeDemoDatas();
        	for(ExportTable each : datas){
        		word.createDocContext(file, each, document, bfChinese, contextFont);
        	}
            document.close();  
        } catch (DocumentException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
	}
	
	public static void makeDBFile(String docTargetPath,String fontFilePath, List<ExportTable> datas)throws Exception {
		TestExport word = new TestExport();
		String file = docTargetPath.trim();  
        //设置纸张大小  
        Document document = new Document(PageSize.A4);  
        //建立一个书写器，与document对象关联  
        RtfWriter2.getInstance(document, new FileOutputStream(file));  
        document.open();  
        //设置中文字体  
//        BaseFont bfChinese = BaseFont.createFont("/home/bigtree/workspace/simsun.ttc,0", BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
        BaseFont bfChinese = BaseFont.createFont(fontFilePath.trim()+",0", BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
        //标题字体风格  
        Font titleFont = new Font(bfChinese,12,Font.BOLD);  
        //正文字体风格  
        Font contextFont = new Font(bfChinese,10,Font.NORMAL);  
        Paragraph title = new Paragraph("数据库表结构设计说明书");  
        //设置标题格式对齐方式  
        title.setAlignment(Element.ALIGN_CENTER);  
        title.setFont(titleFont);  
        document.add(title);
        try {  
        	for(ExportTable each : datas){
        		word.createDocContext(file, each, document, bfChinese, contextFont);
        	}
            document.close();  
        } catch (DocumentException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
	}
	
	
	
	public void createDocContext(String file,ExportTable eachTable, Document document, BaseFont bfChinese, Font contextFont)throws DocumentException, IOException{
		Paragraph context;
		if(eachTable.getTableComment() == null){
			context = new Paragraph(eachTable.getTableName());
		}else{
			context = new Paragraph(eachTable.getTableName()+"("+eachTable.getTableComment()+")");
		}
          
        context.setAlignment(Element.ALIGN_LEFT);  
        context.setFont(contextFont);  
        //段间距
        context.setSpacingBefore(10);
        //设置第一行空的列数  
//        context.setFirstLineIndent(4);  
        context.setSpacingAfter(0);
        document.add(context);  
        //设置Table表格,创建一个三列的表格  
        Table table = new Table(7);  
        int width[] = {5,20,20,15,10,10,10};//设置每列宽度比例  
        table.setWidths(width);  
        table.setWidth(90);//占页面宽度比例  
        table.setAlignment(Element.ALIGN_CENTER);//居中  
        table.setAlignment(Element.ALIGN_MIDDLE);//垂直居中  
        table.setAutoFillEmptyCells(true);//自动填满  
        table.setBorderWidth(1);//边框宽度  
        //设置表头  
        //序号	字段名称	字段描述	字段类型	长度	允许空	缺省值
        Cell haderCell1 = new Cell("序号");
        Cell haderCell2 = new Cell("字段名称");
        Cell haderCell3 = new Cell("字段描述");
        Cell haderCell4 = new Cell("字段类型");
        Cell haderCell5 = new Cell("长度");
        Cell haderCell6 = new Cell("允许空");
        Cell haderCell7 = new Cell("缺省值");
        haderCell1.setBackgroundColor(new Color(166, 166, 166));
        haderCell2.setBackgroundColor(new Color(166, 166, 166));
        haderCell3.setBackgroundColor(new Color(166, 166, 166));
        haderCell4.setBackgroundColor(new Color(166, 166, 166));
        haderCell5.setBackgroundColor(new Color(166, 166, 166));
        haderCell6.setBackgroundColor(new Color(166, 166, 166));
        haderCell7.setBackgroundColor(new Color(166, 166, 166));
        haderCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
        haderCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
        haderCell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
        haderCell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
        haderCell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
        haderCell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
        haderCell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
        haderCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        haderCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        haderCell3.setHorizontalAlignment(Element.ALIGN_CENTER);
        haderCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
        haderCell5.setHorizontalAlignment(Element.ALIGN_CENTER);
        haderCell6.setHorizontalAlignment(Element.ALIGN_CENTER);
        haderCell7.setHorizontalAlignment(Element.ALIGN_CENTER);
        haderCell1.setHeader(true);
        haderCell2.setHeader(true);
        haderCell3.setHeader(true);
        haderCell4.setHeader(true);
        haderCell5.setHeader(true);
        haderCell6.setHeader(true);
        haderCell7.setHeader(true);
        table.addCell(haderCell1);
        table.addCell(haderCell2);
        table.addCell(haderCell3);
        table.addCell(haderCell4);
        table.addCell(haderCell5);
        table.addCell(haderCell6);
        table.addCell(haderCell7);
        table.endHeaders();  
        
        Font fontChinese = new Font(bfChinese,12,Font.NORMAL,Color.BLACK);  
        for(int i =0; i < eachTable.getColumns().size(); i++){
        	ExportColumn eachColumn = eachTable.getColumns().get(i);
        	Cell seq = new Cell(""+(i+1));
        	seq.setVerticalAlignment(Element.ALIGN_MIDDLE);
        	seq.setHorizontalAlignment(Element.ALIGN_CENTER);
        	table.addCell(seq);
        	
        	Cell nameCell = new Cell(new Paragraph(eachColumn.getColumnName(),fontChinese));
        	nameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        	nameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	table.addCell(nameCell);
        	
        	Cell commentCell = new Cell(new Paragraph(eachColumn.getColumnComment()));
        	commentCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        	commentCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	table.addCell(commentCell);
        	
        	Cell typeCell = new Cell(eachColumn.getColumnType());
        	typeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        	typeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	table.addCell(typeCell);
        	
        	Cell lenCell = new Cell(eachColumn.getColumnLength());
        	lenCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        	lenCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	table.addCell(lenCell);
        	
        	Cell nullCell = new Cell(eachColumn.getNallable());
        	nullCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        	nullCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	table.addCell(nullCell);
        	
        	
        	Cell defaultCell = new Cell(eachColumn.getDefaultValue());
        	defaultCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        	defaultCell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	table.addCell(defaultCell);
        }
        document.add(table);  
    }  
	
	
	/**
	 * 模拟构造两张表
	 * @return
	 */
	public static List<ExportTable> makeDemoDatas(){
		ExportTable table1 = new ExportTable();
        table1.setTableName("T_PLATFORM_USER_FUND");
        table1.setTableComment("用户基金表");
        ExportColumn column1 = new ExportColumn();
        ExportColumn column2 = new ExportColumn();
        ExportColumn column3 = new ExportColumn();
        column1.setColumnName("ID");
        column1.setColumnComment("主键");
        column1.setColumnLength("32");
        column1.setColumnType("VARCHAR2");
        column1.setNallable("Y");
        column2.setColumnName("U_ID");
        column2.setColumnComment("用户标识");
        column2.setColumnLength("11");
        column2.setColumnType("VARCHAR2");
        column2.setNallable("N");
        column3.setColumnName("PRODUCT_NO");
        column3.setColumnComment("产品编号");
        column3.setColumnLength("20");
        column3.setColumnType("VARCHAR2");
        column3.setNallable("Y");
        List<ExportColumn> colums1 = new ArrayList<>();
        colums1.add(column1);
        colums1.add(column2);
        colums1.add(column3);
        table1.setColumns(colums1);
        
        ExportTable table2 = new ExportTable();
        table2.setTableName("T_PAY_PAYMENT");
        table2.setTableComment("支付流水表");
        ExportColumn column4 = new ExportColumn();
        ExportColumn column5 = new ExportColumn();
        ExportColumn column6 = new ExportColumn();
        column4.setColumnName("ID");
        column4.setColumnComment("主键");
        column4.setColumnLength("32");
        column4.setColumnType("VARCHAR2");
        column4.setNallable("Y");
        column5.setColumnName("PAY_SEQ_NO");
        column5.setColumnComment("支付流水号(21位支付流水号，如：15040913+1+1001+8位随机数)");
        column5.setColumnLength("24");
        column5.setColumnType("NUMBER");
        column5.setNallable("N");
        column6.setColumnName("PAY_TYPE");
        column6.setColumnComment("支付类型（1=支付宝收款，2=支付宝银行付款）");
        column6.setColumnLength("20");
        column6.setColumnType("VARCHAR2");
        column6.setNallable("Y");
        List<ExportColumn> colums2 = new ArrayList<>();
        colums2.add(column4);
        colums2.add(column5);
        colums2.add(column6);
        table2.setColumns(colums2);
        List<ExportTable> lists = new ArrayList<>();
        lists.add(table1);
        lists.add(table2);
        return lists;
	}

}
